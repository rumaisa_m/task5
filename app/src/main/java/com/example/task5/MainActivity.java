package com.example.task5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String pslList [] = {"Karachi Kings","Islamabad United","Lahore Qalandar","Peshawar Zalmi", "Quetta Gladiators", "Multan Sultan"};
    int pslImages [] = {R.drawable.karachikings,R.drawable.islamabadunited,R.drawable.lahoreqalandar, R.drawable.peshawarzalmi, R.drawable.quettagladiators,R.drawable.multansultan};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        ArrayList<Row> arrayList = new ArrayList<Row>();
        for (int i = 0;i < pslList.length; i++ ){
            arrayList.add(new Row(pslImages[i],pslList[i]));
        }


        CustomAdapter customAdapter = new CustomAdapter(this,R.layout.listrow,arrayList);
        listView.setAdapter(customAdapter);
    }
}
